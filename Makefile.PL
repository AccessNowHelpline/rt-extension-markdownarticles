use inc::Module::Install;
use utf8;

RTx 'RT-Extension-MarkdownArticles';
license 'gplv3';
repository 'https://gitlab.com/AccessNowHelpline/rt-extension-markdownarticles';

requires_rt('4.4.0');

sign;
WriteAll();
