use strict;
use warnings;
package RT::Extension::MarkdownArticles;

our $VERSION = '0.01';

=head1 NAME

RT-Extension-MarkdownArticles - Displays and includes articles as rich text

=head1 DESCRIPTION

This extension allows you to selectively render Markdown in article
content as rich text. It adds a custom field to articles that allows
you to select which Markdown rendering engine to use for that article.

The web interface will display Markdown articles as rich
text. Articles included in rich text mail will be rendered as HTML in
the text/html part.

=head1 RT VERSION

Works with RT 4.4

=head1 INSTALLATION

=over

=item C<perl Makefile.PL>

=item C<make>

=item C<make install>

May need root permissions

=item C<make initdb>

This step creates the Markdown engine custom field on articles. Currently, only the CommonMark rendering engine is supported.

=item Edit your F</opt/rt4/etc/RT_SiteConfig.pm>

If you are using RT 4.2 or greater, add this line:

    Plugin('RT::Extension::MarkdownArticles');

=item Clear your mason cache

    rm -rf /opt/rt4/var/mason_data/obj

=item Restart your webserver

=back

Follow the steps in L</"CONFIGURATION"> to complete installation.

=head1 CONFIGURATION

In addition the steps in L</"INSTALLATION">, you must also define C<%MarkdownArticlesFlavors>:

 # In RT_SiteConfig.pm, add entries for each Markdown rendering engine
 # you want to use. For example:

 Set(%MarkdownArticlesFlavors,
        Markdown => {
                Path => '/usr/bin/markdown',
                Options => '',
        },
        Kramdown => {
                Path => '/usr/bin/kramdown',
                Options => '--input kramdown --output html',
        },
 );

 # You must define a flavor for each Markdown rendering engine value
 # defined in the 'Markdown Flavor' custom field applied to articles.

=head1 AUTHOR

Countercontrol Technology LLC E<lt>brian@countercontrol.techE<gt>

=head1 BUGS

All bugs should be reported via the web at

    L<gitlab.com|https://gitlab.com/AccessNowHelpline/rt-extension-markdownarticles/issues>.

=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2019 by Access Now Inc

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007

=cut

# User overridable options
$RT::Config::META{MarkdownArticlesFlavors} = {
    Markdown => {
        Path => '/usr/bin/markdown',
        Options => '',
    },
    Kramdown => {
        Path => '/usr/bin/kramdown',
        Options => '--input kramdown --output html',
    },
};

1;
